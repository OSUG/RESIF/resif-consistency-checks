#!/usr/bin/env python3

"""
Vérifier la cohérence en partant de la table rall vis à vis de la base seedtree et wfcatalog
Pour chaque fichier dans rall
1. On vérifie qu'il existe dans la base seetree
2. On vérifie qu'il existe dans wfcatalog
"""

import time
import logging
import os
import sys
import json
from signal import signal, SIGINT
import psycopg2
from tqdm import tqdm
from fdsnnetextender import fdsnnetextender

logging.basicConfig(level=logging.INFO)
net_code_extender = fdsnnetextender.FdsnNetExtender()
seedtree_uri = "postgresql://seedtree5@resif-pgprod.u-ga.fr:6432/seedtree5"
inventory_uri = "postgresql://resifinv_ro@resif-pgprod.u-ga.fr:5432/resifInv-Prod"
result = {"not_in_seedtree": {"description": "File is in rall but not in seedtree. Should be indexed.", "records":[]}}
# TODO connection seedtree sur pgpool

_dumpfile = f"results_{time.strftime('%Y.%j')}_{os.getpid()}.json"

def save_results():
    with open(_dumpfile, 'w') as fp:
        json.dump(result, fp)

def handler(signal_received, frame):
    # Handle any cleanup here
    print('SIGINT or CTRL-C detected. Exiting gracefully')
    save_results()
    sys.exit(0)

def get_seedtree_schema(extnet):
    """
    Renvoi le nom du schéma seedtree correspondant au code réseau étendu
    ex. get_seedtree_schema("XC2040")
    """
    return f"_{extnet.lower()}"

def is_in_seedtree(source_file):
    """
    Teste si le nom de fichier est référencé dans seedtree
    """
    r = False
    (n, _, _, _ , _, y, _) = source_file.split('.')
    # On cherche le code réseau étendu
    extnet = net_code_extender.extend(net=n, date_string=str(y))
    # On cherche aussi le nom du schéma de seedtree
    schema = get_seedtree_schema(extnet)
    try:
        with psycopg2.connect(seedtree_uri) as conn:
            with conn.cursor() as curs:
                request = f"SELECT 1 from {schema}.files where basename = %s"
                logging.debug(curs.mogrify(request, (source_file,)))
                curs.execute(request, (source_file,))
                r = (curs.rowcount > 0)
                logging.debug("Is in seedtree ? %s", result)
    except Exception as e:
        logging.error("Porblème avec la base")
        logging.error(e)
        save_results()
        time.sleep(60)
    return r


def start_consistency_check():
    """
    Récupération de la liste des fichiers dans la table rall et test sur seedtree
    """
    with psycopg2.connect(inventory_uri) as conn:
        with conn.cursor() as curs:
            curs.execute("SELECT source_file from rall where network_id !=0 and station_id !=0 and channel_id !=0")
            for source_file in tqdm(curs, total=curs.rowcount):
                if not is_in_seedtree(source_file[0]):
                    result["not_in_seedtree"]["records"].append(source_file[0])
    with open(_dumpfile, 'w') as fp:
        json.dump(result, fp)



if __name__ == "__main__":
    # Tell Python to run the handler() function when SIGINT is recieved
    signal(SIGINT, handler)
    start_consistency_check()
