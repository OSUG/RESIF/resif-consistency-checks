#!/usr/bin/env python3

# Pour chaque fichier dans l'archive des données validées :
# - si pas dans resifInv, alors nettoyage
#
import time
import logging
import os
import sys
import json
from signal import signal, SIGINT
import psycopg2
from tqdm import tqdm
from datetime import datetime
import glob

logging.basicConfig(level=logging.INFO)
seedtree_uri = "postgresql://seedtree5@resif-pgprod.u-ga.fr:6432/seedtree5"
inventory_uri = "postgresql://resifinv_ro@resif-pgprod.u-ga.fr:5432/resifInv-Prod"
result = {"run_date": datetime.now().strftime('%Y-%m-%d'),
          "in_archive_not_in_base": {"description": "File is in archive but is not in inventory. Should be cleaned.", "records":[]}}
archive_dirs=["/mnt/nfs/summer/validated_seismic_data", "/mnt/nfs/summeR/cold_validated_seismic_data"]

_dumpfile = f"results_{time.strftime('%Y.%j')}_{os.getpid()}.json"

def save_result():
    with open(_dumpfile, 'w') as fp:
        json.dump(result, fp)

def handler(signal_received, frame):
    # Handle any cleanup here
    logging.info('SIGINT or CTRL-C detected. Exiting gracefully')
    save_result()
    sys.exit(0)

def scan_archive():
    """
    Parcours des archives et test pour chaque fichier
    """
    for archive_dir in archive_dirs:
        for filename in tqdm(glob.iglob(archive_dir + '/**/**', recursive=True)):
            if os.path.isfile(filename):
                try:
                    with psycopg2.connect(inventory_uri) as conn:
                        with conn.cursor() as curs:
                            curs.execute("SELECT 1 from rall where source_file = %s", (filename.split('/')[-1],))
                            if curs.rowcount == 0:
                                logging.debug(f"File {filename} not in inventory")
                                result["in_archive_not_in_base"]["records"].append(filename)
                            else:
                                logging.debug(f"File {filename} found in inventory")
                except psycopg2.OperationalError as e:
                    logging.error("Problème avec la base de données")
                    logging.error("e")
                    logging.error("Nouvel essai dans 60s")
                    save_result()
                    time.sleep(60)
                except Exception as e:
                    logging.error("Problème non prévu")
                    logging.error(e)
                    save_result()
                    raise

if __name__ == "__main__":
    # Tell Python to run the handler() function when SIGINT is recieved
    signal(SIGINT, handler)
    scan_archive()
    with open(_dumpfile, 'w') as fp:
        json.dump(result, fp)
