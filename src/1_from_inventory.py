#!/usr/bin/env python3

"""
Pour chaque objet de l'inventaire (rall et rbud)
  - Si présent dans l'archive (avec auto-montage) :
    - calculer un hash et l'ajouter en base
  - Si absent de l'archive
    - supprimer l'entrée en base de donnée
  - Si absent de seedtree :
    - ajouter à la base seedtree
"""

import time
import datetime
import json
from multiprocessing import Pool
import logging
import os
import psycopg2
import xxhash
import click
from tqdm import tqdm

from fdsnextender import fdsnextender

net_code_extender = fdsnextender.FdsnExtender()

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger()

archive_dir = '/mnt/auto/archive/data/bynet/'
bud_dir = '/mnt/auto/archive/rawdata/bud/'
psql_string = "user=resifinvdev host=resif-pgpreprod.u-ga.fr port=5432 dbname=resifInv-Preprod"
# First get all objects from table resifInv-rall
#
class References:
    def __init__(self, work_dir='/tmp'):
        self.work_dir = work_dir

        self._dumpfile = f"{self.work_dir}/results_{time.strftime('%Y.%j')}_{os.getpid()}.json"
        self.date = datetime.datetime.now(),
        self.big_results = {
            'phantoms': {
                'description': "Records in database not found on archive",
                'records': [],
            },
            'malformed_source_file': {
                'desciption': "Source file is weird",
                'records': []
            },
            'files': {
                'description': "List of file references ans metadata",
                'records': []
            }
        }

    def load_records(self, filepath):
        try:
            f = open(filepath, 'r')
        except FileNotFoundError as e:
            logger.warning("Not data to reload. Continue.")
            return None
        try:
            self.big_results = json.load(f)
        except Error as e:
            logger.error("Error while loading json data from %s", filepath)
            logger.error("e")

    def merge(self, other):
        logging.debug(other)
        for key,val in other.big_results.items():
            if key in self.big_results:
                self.big_results[key]['records'] += val['records']
            else:
                self.big_results[key] = val

    def add_record(self, key, record):
        logger.debug("Adding %s in the %s references", record, key)

        if key in self.big_results:
            self.big_results[key]['records'].append(record)
        else:
            raise ValueError(f"Key {key} does not exist in the references")

    def __repr__(self):
        return str(self.big_results)

    def dump(self):
        """
        Serialize the results as json file in work_dir
        """
        os.makedirs(self.work_dir, exist_ok=True)
        with open(self._dumpfile, 'w') as f:
            json.dump(self.big_results, f)


def from_inventory_check(record):
    """
    Check consistency.
    Ref is an array containing:
    [network, year, station, channel, file, id, archive ]
    Consistency is OK if:
    - the record
    - a file exist on the archive for this record
    """
    reference = References()
    network = record[0]
    year = record[1]
    station = record[2]
    channel = record[3]
    filename = record[4]
    archive = record[6]
    is_orphan = record[7]
    try:
        # INFO:root:Malformed ('RD', 2021, 'FLAF', 'BHE', 'RD.FLAF..BHE.D.2021.020', 8430896, 'rall')
        (network, station, _ , channel, _ , year, _) = record[4].split('.')
    except Exception as e:
        logger.info("Malformed %s", record)
        reference.add_record("malformed_source_file", {"id": record[5], "archive": archive, "source_file": filename})
        return reference

    try:
        network = net_code_extender.extend(net=network, year=year)
    except Exception as e:
        logger.error(e)
        return reference

    logger.debug(record)
    if archive == 'rall':
        path = f"{archive_dir}/{network}/{year}/{station}/{channel}.D/{filename}"
    elif archive == 'rbud':
        path = f"{bud_dir}/{year}/{network}/{station}/{channel}.D/{filename}"
    else:
        logger.error("Archive type %s should be rall or rbud", archive)
        return reference
    logger.debug(path)

    try:
        statinfo = os.stat(path)
        logger.debug("  %s", statinfo)
    except FileNotFoundError:
        logger.debug("  File %s referenced as %s does not exist", path, record[5])
        # This is a phantom file, add it to the list
        reference.add_record('phantoms', {'id': record[5], 'path': path, 'archive': archive, 'is_orphan': is_orphan})
        return reference
    if archive == 'rall':
        file_hash = xxhash.xxh64(open(path, 'rb').read()).hexdigest()
        logger.debug("  %s", file_hash)
        created_at = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(statinfo.st_mtime))
        with psycopg2.connect(psql_string) as conn:
            with conn.cursor() as update_cursor:
                update_cursor.execute("UPDATE rall SET xxh64 = %s, size = %s, created_at = %s WHERE rall_id = %s",
                                        (file_hash, statinfo.st_size, created_at, record[5],))
    return reference


def orphan_files_check(record):
    """
    From an inventory record (filename, identifier, archive), check the existence of the corresponding file.
    If file does not exist, put in the referneces.
    If file exists,
      If there is no record for the same filename with every network_id and station_id and channel_id set, add to reference

    """
    logger.debug("")
    logger.debug("")
    logger.debug("")
    logger.debug("==== %s ====", record)
    reference = References()
    # filename exemple: G.UNM.00.VHE.D.2014.039
    try:
        (network, station, _ , channel, _ , year, _) = record[0].split('.')
    except Exception:
        logger.info("Malformed %s", record)
        return reference
    identifier = record[1]
    filename = record[0]
    archive = record[2]
    if archive == 'rall':
        path = f"{archive_dir}/{network}/{year}/{station}/{channel}.D/{filename}"
    elif archive == 'rbud':
        path = f"{bud_dir}/{year}/{network}/{station}/{channel}.D/{filename}"
    else:
        logger.error("Archive type %s should be rall or rbud", archive)
        return reference

    logger.debug(path)
    try:
        statinfo = os.stat(path)
        logger.debug(statinfo)
    except FileNotFoundError:
        logger.debug("  File %s referenced as %s does not exist", path, identifier)
        reference.add_record('orphan_refs_to_delete', {'id': identifier,'archive': archive})
        return reference

#     # Search for a good reference of this file in the table
#     with psycopg2.connect(psql_string) as conn:
#         with conn.cursor() as curs:
#             curs.execute(f"select count(*) from {archive} where source_file=%s and network_id != 0 and station_id != 0 and channel_id != 0", (filename,))
#             count = curs.fetchone()[0]
#             logger.debug("Occurences of %s: %s", filename, count)
#             if count > 0:
#                 reference.add_record('forgotten_orphans', {'path': filename, 'archive': archive})
    return reference



def consistency_from_inventory(tablename, limit):
    """
    Collecte toutes les entrées dans les tables d'inventaire afin de les passer aux fonctions de vérification en parallèle.
    """
    results = References()
    if tablename == 'rall':
        select_statement = "case when n.endtime='infinity' then n.network else n.network||n.start_year end, r.year, s.station, c.channel, r.source_file, r.rall_id, 'rall', r.channel_id=0"
        where_statement = "and r.xxh64 is NULL and r.size is NULL"
    elif tablename == 'rbud':
        select_statement = "r.year, case when n.endtime='infinity' then n.network else n.network||n.start_year end, s.station, c.channel, r.source_file, r.rbud_id, 'rbud', r.channel_id=0"
        where_statement = ""
    else:
        logger.error("Table %s is not rall or rbud")
        return results

    with psycopg2.connect(psql_string) as conn:
        with conn.cursor() as curs:
            logger.info("Issuing large database request. Please wait")
            curs.execute(f"select {select_statement} from {tablename} as r, networks as n, station as s, channel as c where r.network_id=n.network_id and r.station_id=s.station_id and r.channel_id=c.channel_id {where_statement} limit {limit}")
            records = curs.fetchall()
    with Pool(processes=16) as p:
        for res in tqdm(p.imap_unordered(from_inventory_check, records, 100), total = len(records)):
            results.merge(res)
    return results

@click.command()
@click.option('--runmode', envvar='RUNMODE', type=click.Choice(['production', 'preprod'], case_sensitive=False))
@click.option('--work-dir', type=click.Path(dir_okay=True, file_okay=False, writable=True, resolve_path=True), default='output')
@click.option('--limit', help="Limit request results", default=100)
@click.option('--verbose')
def cli(runmode, work_dir, limit, verbose):
    global psql_string
    if runmode == "preprod":
        psql_string = "user=resifinvdev host=resif-pgpreprod.u-ga.fr port=5432 dbname=resifInv-Preprod"
    elif runmode == "production":
        psql_string = "user=resifinvprod host=129.88.201.45 port=5432 dbname=resifInv-Prod"

    global net_code_extender
    net_code_extender = fdsnextender.FdsnExtender(dburi=psql_string)
    all_results = References(work_dir)
    # logger.info("Checking all inventroy in rbud")
    # all_results.merge(consistency_from_inventory('rbud', limit))
    # all_results.dump()
    logger.info("Checking all inventroy in rall")
    all_results.merge(consistency_from_inventory('rall', limit))
    all_results.dump()
    print(f"Results written to {all_results._dumpfile}")

if __name__ == '__main__':
    cli()
