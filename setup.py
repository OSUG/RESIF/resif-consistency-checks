#!/usr/bin/env python3
# encoding: utf8
import os
from io import open
from setuptools import find_packages, setup

current_path = os.path.abspath(os.path.dirname(__file__))

try:
    with open(os.path.join(current_path, "resif-consistency-checks", "__init__.py"), "r") as f:
        for line in f:
            if line.startswith("__version__"):
                version = line.strip().split("=")[1].strip(" '\"")
                break
        else:
            version = "0.0.1"
except FileNotFoundError:
    version = "0.0.1"

try:
    with open(os.path.join(current_path, "README.md"), "r", encoding="utf-8") as f:
        readme = f.read()
except FileNotFoundError:
    readme = ""

REQUIRES = ["click", "psycopg2", "tqdm", "xxhash"]

description = (
    """Checks consistency in resif databases"""
)

kwargs = {
    "name": "resif-consistency-checks",
    "version": version,
    "description": "SUMMER monitoring",
    "long_description": readme,
    "author": "Jonathan Schaeffer",
    "author_email": "dc@resif.fr",
    "maintainer": "Résif-DC",
    "maintainer_email": "dc@resif.fr",
    "url": "https://gricad-gitlab.univ-grenoble-alpes.fr/OSUG/RESIF/resif-consistency-checks",
    "license": "GPLv3",
    "classifiers": [
        "Development Status :: 4 - Beta",
        "Intended Audience :: Developers",
        "License :: OSI Approved :: GNU General Public License v3 (GPLv3)",
        "Natural Language :: English",
        "Operating System :: OS Independent",
        "Programming Language :: Python :: 3",
    ],
    "install_requires": REQUIRES,
    "tests_require": ["coverage", "pytest"],
    "packages": find_packages(exclude=("tests", "tests.*")),
    "include_package_data": True,
    "entry_points": """
        [console_scripts]
        resif-consistency-checks-1=src.1_from_inventory:cli
    """,
}

setup(**kwargs)
